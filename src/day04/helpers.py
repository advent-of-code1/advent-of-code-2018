from datetime import datetime


class Event:
    def __init__(self, dt_str, action):
        self.dt = datetime.strptime(dt_str, '%Y-%m-%d %H:%M')
        self.action = action

    def __str__(self):
        return '{} - {}'.format(self.dt, self.action)

    def __repr__(self):
        return str(self)


class Day:
    def __init__(self, guard, date):
        self.guard = guard
        self.date = date

        self.events = []

    def add_event(self, event):
        self.events.append(event)

    def __str__(self):
        return '{} - {}'.format(self.guard, self.date)

    def __repr__(self):
        return str(self)

    def sleeping_minutes(self):
        sleeping_minutes = []
        for i in range(0, len(self.events), 2):
            falls_asleep_event = self.events[i]
            wakes_up_event = self.events[i+1]
            for sleeping_minute in range(falls_asleep_event.dt.minute, wakes_up_event.dt.minute):
                sleeping_minutes.append(sleeping_minute)
        return sleeping_minutes



def load_input(filename):
    with open(filename) as f:
        content = f.readlines()
    return [x.strip() for x in content]


def parse_content(content):
    events = []
    for line in content:
        dt_str = line.split(']')[0].strip('[')
        action_str = line.split('] ')[1]
        events.append(Event(dt_str, action_str))
    return events


def events_in_days(events):
    days = []
    day = None
    for i in range(0, len(events)):
        event = events[i]

        if '#' in event.action:
            if day:
                days.append(day)
            guard_id = int(event.action.split('#')[1].split(' ')[0])
            date = events[i+1].dt.date()
            day = Day(guard_id, date)
        else:
            day.add_event(event)

    days.append(day)
    return days
