import helpers

input_file_name = './src/day04/inputs/test_input_1'

content = helpers.load_input(input_file_name)

events = helpers.parse_content(content)

days = helpers.events_in_days(events)

print(days[0].sleeping_minutes())

import pdb
pdb.set_trace()