import helpers

input_file_name = './src/day03/inputs/test_input_1'

content = helpers.load_input(input_file_name)

fabrics = helpers.parse_input(content)
fabrics_len = len(fabrics)

claim_points = {}
for i in range(0, fabrics_len):
    print('{}/{}'.format(i, fabrics_len))
    for j in range(i+1, fabrics_len):
        f1 = fabrics[i]
        f2 = fabrics[j]
        intersection = f1.shapely_polygon.intersection(f2.shapely_polygon)
        if not intersection.bounds:
            continue

        x_min = int(intersection.bounds[0])
        y_min = int(intersection.bounds[1])
        x_max = int(intersection.bounds[2])
        y_max = int(intersection.bounds[3])
        for x in range(x_min, x_max):
            for y in range(y_min, y_max):
                point = (x, y)
                if point not in claim_points:
                    claim_points[point] = 2
                else:
                    claim_points[point] += 1

print(len(claim_points.keys()))