from shapely.geometry import Polygon, mapping


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return '({},{})'.format(self.x, self.y)

    def __repr__(self):
        return str(self)


class Rectangle:
    def __init__(self, id, p1, p2, p3, p4):
        self.id = id
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.p4 = p4

        self.shapely_polygon = Polygon([(p1.x, p1.y), (p2.x, p2.y), (p3.x, p3.y), (p4.x, p4.y)])

    def __str__(self):
        return '{}, {}, {}, {}'.format(self.p1, self.p2, self.p3, self.p4)

    def __repr__(self):
        return str(self)


def load_input(filename):
    with open(filename) as f:
        content = f.readlines()
    return [x.strip() for x in content]


def parse_input(content):
    fabrics = []
    for line in content:
        id = line.split(' @ ')[0][1:]
        info = line.split(' @ ')[1]
        coords = info.split(': ')[0]
        dims = info.split(': ')[1]
        x = int(coords.split(',')[0])
        y = int(coords.split(',')[1])
        l = int(dims.split('x')[0])
        h = int(dims.split('x')[1])
        fabrics.append(
            Rectangle(
                id,
                Point(x, y),
                Point(x+l, y),
                Point(x+l, y+h),
                Point(x, y+h)
            )
        )

    return fabrics
