import helpers

input_file_name = './src/day03/inputs/input'

content = helpers.load_input(input_file_name)

fabrics = helpers.parse_input(content)
fabrics_len = len(fabrics)

counter = 0
for f1 in fabrics:
    counter += 1
    print('{}/{}'.format(counter, fabrics_len))
    intersected = False
    for f2 in fabrics:
        if f1 == f2:
            continue
        intersection = f1.shapely_polygon.intersection(f2.shapely_polygon)
        if not intersection.area:
            continue
        intersected = True
    if not intersected:
        print(f1.id)
        exit()
