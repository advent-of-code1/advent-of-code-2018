def load_input(filename):
    with open(filename) as f:
        content = f.readlines()
    return [x.strip() for x in content]
