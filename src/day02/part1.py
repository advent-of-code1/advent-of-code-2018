from collections import Counter

import helpers

input_file_name = './src/day02/inputs/input'

content = helpers.load_input(input_file_name)

twos = 0
threes = 0
for line in content:
    counter = Counter(line)

    seen_two = False
    seen_three = False
    for value in counter.values():
        if value == 2 and not seen_two:
            twos += 1
            seen_two = True
        if value == 3 and not seen_three:
            threes += 1
            seen_three = True

print(twos*threes)
