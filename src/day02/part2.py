import helpers

def has_1_diff_char(line_1, line_2):
    diffs = 0
    for i in range(len(line_1)):
        char_1 = line_1[i]
        char_2 = line_2[i]

        if char_1 != char_2:
            diffs += 1

        if diffs > 1:
            return False

    if diffs == 1:
        return True

    return False


def calculate_result(line_1, line_2):
    result = ''
    for i in range(len(line_1)):
        char_1 = line_1[i]
        char_2 = line_2[i]

        if char_1 == char_2:
            result += char_1

    return result


input_file_name = './src/day02/inputs/input'

content = helpers.load_input(input_file_name)

content_len = len(content)

for i in range(0, content_len):
    print('{}/{}'.format(i, content_len))
    for j in range(i+1, content_len):
        line_1 = content[i]
        line_2 = content[j]
        if has_1_diff_char(line_1, line_2):
            result = calculate_result(line_1, line_2)
            print(result)
            exit()
