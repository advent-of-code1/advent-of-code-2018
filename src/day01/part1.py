import helpers

input_file_name = './src/day01/inputs/input'

content = helpers.load_input(input_file_name)

total_freq = 0
for freq in content:
    total_freq += freq

print(total_freq)
