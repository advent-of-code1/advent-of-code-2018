import helpers

input_file_name = './src/day01/inputs/input'

frequencies = helpers.load_input(input_file_name)

unique_frequencies = [0]
total_freq = 0
current_position = 0
parsed_file_num = 0
while True:
    current_freq = frequencies[current_position]
    total_freq += current_freq
    if total_freq in unique_frequencies:
        break
    unique_frequencies.append(total_freq)

    current_position += 1
    if current_position == len(frequencies):
        current_position = 0
        parsed_file_num += 1
        print('Times parsed file: {}'.format(parsed_file_num))

print(total_freq)
