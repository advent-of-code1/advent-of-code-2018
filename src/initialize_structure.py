import logging

import os


logger = logging.getLogger('Initialize structure')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

# Config
src_dir = './src'
current_year = 2019
number_of_days = 25


# Script
def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]


logger.log(logging.INFO, 'Started.')

dirs_in_src_dir = get_immediate_subdirectories(src_dir)

for day in range(1, number_of_days+1):
    str_day = str(day).zfill(2)

    new_dir_name = 'day{}'.format(str_day)

    if new_dir_name in dirs_in_src_dir:
        logger.log(logging.INFO, 'Day {}: Directory already exists.'.format(str_day))
        continue

    new_dir_path = '{}/{}'.format(src_dir, new_dir_name)
    new_dir_inputs_path = '{}/inputs'.format(new_dir_path)
    new_helpers_path = '{}/helpers.py'.format(new_dir_path)
    new_part1_path = '{}/part1.py'.format(new_dir_path)
    new_part2_path = '{}/part2.py'.format(new_dir_path)

    os.mkdir(new_dir_path)
    os.mkdir(new_dir_inputs_path)
    os.mknod(new_helpers_path)
    os.mknod(new_part1_path)
    os.mknod(new_part2_path)

    logger.log(logging.INFO, 'Day {}: Directory structure created.'.format(str_day))
